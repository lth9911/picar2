import RPi.GPIO as GPIO

import sys, tty, termios, time

GPIO.setmode(GPIO.BOARD)
#motor pins
motor1_in1_pin = 13
motor1_in2_pin = 15
motor2_in1_pin = 18
motor2_in2_pin = 22 
ENA = 7
ENB = 11
GPIO.setup(ENA, GPIO.OUT)
GPIO.setup(ENB, GPIO.OUT)
GPIO.setup(motor1_in1_pin, GPIO.OUT)
GPIO.setup(motor1_in2_pin, GPIO.OUT)
GPIO.setup(motor2_in1_pin, GPIO.OUT)
GPIO.setup(motor2_in2_pin, GPIO.OUT)
GPIO.output(ENA, True)
GPIO.output(ENB, True)

#LED
led_in1_pin = 35
led_in2_pin = 37
GPIO.setup(led_in1_pin, GPIO.OUT)
GPIO.setup(led_in2_pin, GPIO.OUT)

#motor1 = GPIO.PWM(7,100)
#motor1.start(50)
#motor1.ChangeDutyCycle(0)

#get keyboard
def getch():
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(sys.stdin.fileno())
		ch = sys.stdin.read(1)
	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	
	return ch

#move functions
def forward():
	GPIO.output(ENA, True)
	GPIO.output(motor1_in1_pin, True)
	GPIO.output(motor1_in2_pin, False)
	print 'fwd'

def reverse():
	GPIO.output(ENA, True)
	GPIO.output(motor1_in1_pin, False)
	GPIO.output(motor1_in2_pin, True)
	print 'rvs'

def turnRight():
	GPIO.output(ENB, True)
	GPIO.output(motor2_in1_pin, True)
	GPIO.output(motor2_in2_pin, False)
	print 'right'

def turnLeft():
        GPIO.output(ENB, True)
        GPIO.output(motor2_in1_pin, False)
        GPIO.output(motor2_in2_pin, True)
	print 'left'

def steering(direction):
	global wheelStatus
	if direction == 'right':
		if wheelStatus == 'center':
			turnRight()
			wheelStatus = 'right'
		elif wheelStatus == 'left':
			wheelStatus = 'center'
	if direction == 'left':
		if wheelStatus == 'center':
			turnLeft()
			wheelStatus = 'left'
		elif wheelStatus == 'right':
			wheelStatus = 'center'
	

def stop():
	GPIO.output(ENA, True)
	GPIO.output(ENB, True)
	GPIO.output(motor1_in1_pin, False)
	GPIO.output(motor1_in2_pin, False)
	GPIO.output(motor2_in1_pin, False)
        GPIO.output(motor2_in2_pin, False)
	print 'stop'

def led():
	GPIO.output(led_in1_pin, True)
	GPIO.output(led_in2_pin, True)
stop()
wheelStatus = 'center'
while True:
	char = getch()
	if char == 'w':
		forward()
		#motor1.ChangeDutyCycle(99)
	if char == 's':
		#print char
		reverse()
		#motor1.ChangeDutyCycle(99)
	if char == 'a':
		turnLeft()
		#steering('left')
	if char == 'd':
		turnRight()
		#steering('right')
	if char == 'o':
		#print char
		stop()
	if char == 'f':
		led()
	if char == 'x':
		#print char
		break

	#GPIO.PWM(ENA, 100).ChangeDutyCycle(0)
	#GPIO.PWM(ENB, 100).ChangeDutyCycle(0)
	char =''

GPIO.cleanup()
