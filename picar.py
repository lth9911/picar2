#05/13/16

import RPi.GPIO as GPIO
#import webiopi
import time
import sys, tty, termios

#initial
GPIO.setmode(GPIO.BCM)
acceleration = 0
turnacceleration = 0
spotturn = "false"

#Pin Setup
MAXSPEED = 10
MAXSTEERSPEED = 50
motorDriveForwardPin = 13
motorDriveReversePin = 15
motorSteerLeftPin = 18
motorSteerRightPin = 22
motorENA = 7 
motorENB = 11

#Set Pin States
def setup():
	GPIO.setup(motorENA, GPIO.OUT)
	GPIO.setup(motorENB, GPIO.OUT)
	GPIO.setup(motorDriveForwardPin, GPIO.OUT)
	GPIO.setup(motorDriveReversePin, GPIO.OUT)
	GPIO.setup(motorSteerRightPin, GPIO.OUT)
	GPIO.setup(motorSteerLeftPin, GPIO.OUT)

	#set ENA, ENB to high to enable motors
	GPIO.output(motorENA, True)
	GPIO.output(motorENB, True)

def initiate():
	global acceleration
	global turnacceleration
	global motorDriveSpeed
	global motorSteerSpeed
	global speedstep
	global MAXSPEED
	global MAXSTEERSPEED
	global MINSPEED

	spotturn = 'false'
	acceleration = 0
	turnacceleration = 0
	motorDriveSpeed = 0
	motorSteerSpeed = 0
	speedstep = 10
	MAXSPEED = 100
	MAXSTEERSPEED = 50
	MINSPEED = 0

def reverse():
	GPIO.output(motorDriveForwardPin, False)
	GPIO.output(motorDriveReversePin, True)

def forward():
	GPIO.output(motorDriveForwardPin, True)
	GPIO.output(motorDriveReversePin, False)

def left():
	GPIO.output(motorSteerLeftPin, True)
	GPIO.output(motorSteerRightPin, False)

def right():
	GPIO.output(motorSteerLeftPin, False)
	GPIO.output(motorSteerRightPin, True)

def resetSteer():
	GPIO.output(motorSteerLeftPin, False)
	GPIO.output(motorSteerRightPin, False)

def stop():
	GPIO.output(motorDriveForwardPin, False)
	GPIO.output(motorDriveReversePin, False)
	GPIO.output(motorSteerRightPin, False)
	GPIO.output(motorSteerLeftPin, False)

	initiate()

	return 0, 0, 0
def stopSteer():
	GPIO.output(motorSteerRightPin, False)
	GPIO.output(motorSteerLeftPin, False)

def setaccleration(value):
	global motorDriveSpeed
	global motorSteerSpeed
	global MAXSPEED
	global MINSPEED
	global acceleration
	acceleration = acceleration + value
	MINSPEED, MAXSPEED = getMinMaxSpeed()

	#acceleration config.
	if acceleration < -MAXSPEED:
		acceleration = -MAXSPEED

	if acceleration > MAXSPEED:
		acceleration = MAXSPEED

	if acceleration > 0:
		forward()
		motorDriveSpeed = acceleration

	elif acceleration == 0:
		motorDriveSpeed = acceleration
		motorDriveSpeed, motorSteerSpeed, acceleration = stop()

	else: 
		reverse()
		motorDriveSpeed = acceleration * -1

	motorDriveSpeed, motorSteerSpeed = check_motorspeed(motorDriveSpeed, motorSteerSpeed)

def setturnacceleration(value):
	global motorDriveSpeed
	global motorSteerSpeed
	global turnacceleration
	global MAXSTEERSPEED
	global MINSPEED

	turnacceleration = turnacceleration + value
	MINSPEED, MAXSTEERSPEED = getMinMaxSteerSpeed()

	#acceleration config.
	if turnacceleration < -MAXSTEERSPEED:
		turnacceleration = -MAXSTEERSPEED

	if turnacceleration > MAXSTEERSPEED:
		turnacceleration = MAXSTEERSPEED
	
	if turnacceleration > 0:
		left()
		motorSteerSpeed = turnacceleration

	elif turnacceleration == 0:
		motorSteerSpeed = turnacceleration
		motorSteerSpeed, turnacceleration = stopSteer()

	else:
		right()
		motorSteerSpeed = turnacceleration * -1

	motorDriveSpeed, motorSteerSpeed = check_motorspeed(motorDriveSpeed, motorSteerSpeed)

def check_motorspeed(motorDriveSpeed, motorSteerSpeed):
	if (motorDriveSpeed < MINSPEED):
		motorDriveSpeed = MINSPEED

	if (motorDriveSpeed > MAXSPEED):
		motorDriveSpeed = MAXSPEED
		
	if (motorSteerSpeed < MINSPEED):
		motorSteerSpeed = MINSPEED

	if (motorSteerSpeed > MAXSPEED):
		motorSteerSpeed = MAXSPEED	
		
	return motorDriveSpeed, motorSteerSpeed

# Set Min Max Speed
def getMinMaxSpeed():
	minspeed = 0
	maxspeed = 100
	return minspeed, maxspeed

# Set Min Max Speed
def getMinMaxSteerSpeed():
	minspeed = 0
	maxsteerspeed = 50
	return minspeed, maxsteerspeed
	
# Get the motor speed
def getMotorSpeed():
	global motorDriveSpeed
	global motorSteerSpeed
	
	return motorDriveSpeed, motorSteerSpeed

def getMotorSpeedStep():
	return 10	

def getSteerMotorSpeedStep():
	return 50		


#controller

def ButtonForward():
	forwardAcc = 0
	forwardAcc = getMotorSpeedStep()

	setaccleration(forwardAcc)

	motorDriveSpeed, motorSteerSpeed = getMotorSpeed()

	valueDrive = float(motorDriveSpeed)/100

	GPIO.PWM(motorENA, valueDrive)
	print ('fwd')
# @webiopi.macro 
def ButtonReverse():
	backAcc = 0
	backAcc = getSteerMotorSpeedStep()
#
	setaccleration(backAcc*-1)
#
	motorDriveSpeed, motorSteerSpeed = getMotorSpeed()
#
	valueDrive = float(motorDriveSpeed)/100
#
	GPIO.PWM(motorENA, valueDrive)
	print ('rvs')
# @webiopi.macro
def ButtonTurnLeft():
	left()
	GPIO.PWM(motorENB, 0.7)
	time.sleep(1)
	resetSteer()

# @webiopi.macro
def ButtonTurnRight():
	right()
	GPIO.PWM(motorENB, 0.7)
	time.sleep(1)
	resetSteer()
#
# @webiopi.macro
def ButtonStop():
	stop()

# @webiopi.macro
def ButtonTurnLeftOld():
	global motorSteerSpeed
	global motorDriveSpeed
	global speedstep
#
	steerLeftAcc = 0
	steerLeftAcc = getSteerMotorSpeedStep()
#
	setturnacceleration((steerLeftAcc))
#	
	motorDriveSpeed, motorSteerSpeed = getMotorSpeed()
#	
	# percent calculation
	valueDrive = float(motorSteerSpeed)/100
		
	GPIO.PWM(motorENB, valueDrive)
#	
#	#print("LEFT: ",valueL,valueR,spotturn)	
# @webiopi.macro
def ButtonTurnRightOld():
	global motorSteerSpeed
	global motorDriveSpeed
	global speedstep
#
	steerRightAcc = 0
	steerRightAcc = getSteerMotorSpeedStep()
#
	setturnacceleration((steerRightAcc*-1))
#	
	motorDriveSpeed, motorSteerSpeed = getMotorSpeed()
#	
	# percent calculation
	valueDrive = float(motorSteerSpeed)/100
#		
	GPIO.PWM(motorENB, valueDrive)
#	
#	#print("RIGHT: ",valueL,valueR, spotturn)		

#initiate()

#get keyboard
def getch():
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(sys.stdin.fileno())
		ch = sys.stdin.read(1)
	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	
	return ch
if __name__ == "__main__":
	print 'lets drive'
	stop()
	while True:
		setup()
		initiate()
		char = getch()
		if (char == "w"):
			ButtonForward()
		if char == "s":
			ButtonReverse()
		if char == "a":
			ButtonTurnLeft()
		if char == "d":
			ButtonTurnRight()
		if char == "o":
			ButtonStop()
		if char == "x":
			break
	GPIO.cleanup()
